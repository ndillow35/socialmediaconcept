﻿import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';

export class TopNavMenu extends React.Component<{}, {}>{
    public render() {
        return <div className='topNav'>
            <div>
                <ul className='nav navbar-nav'>
                    <li>
                        <NavLink exact to={'/settings'} activeClassName='active'>
                            <span className='glyphicon glyphicon-cog'></span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to={'/profile'} activeClassName='active'>
                            <span className='glyphicon glyphicon-user'></span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to={'/mail'} activeClassName='active'>
                            <span className='glyphicon glyphicon-envelope'></span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to={'/chat'} activeClassName='active'>
                            <span className='glyphicon glyphicon-comment'></span> 
                        </NavLink>
                    </li>
                </ul>
            </div>

        </div>;
    }


} 