import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { MenuItem } from 'reactstrap';

export class SideNavMenu extends React.Component<{}, {}> {
    public render() {
        return <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                <div className='navbar-header'>
                    <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span className='sr-only'>Toggle navigation</span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>                   
                    <Link className='navbar-brand' to={'/'}>
                        <span className='glyphicon glyphicon-asterisk'></span>
                        SocialNetworkConcept
                    </Link>
                </div>
                <div className='clearfix'></div>
                <div className='navbar-collapse collapse'>
                    <ul className='nav navbar-nav'>
                        <li>
                            <NavLink exact to={ '/' } activeClassName='active'>
                                <span className='glyphicon glyphicon-home'></span> Home
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/friends' } activeClassName='active'>
                                <span className='glyphicon glyphicon-user'></span> Friends
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/topics' } activeClassName='active'>
                                <span className='glyphicon glyphicon-th-list'></span> Topics
                            </NavLink>
                        </li>
                        <li>
                            <NavLink exact to={'/blogs'} activeClassName='active'>
                                <span className='glyphicon glyphicon-list-alt'></span> Blogs
                            </NavLink>
                        </li>
                        <li>
                            <NavLink exact to={'/mail'} activeClassName='active'>
                                <span className='glyphicon glyphicon-envelope'></span> Mail
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>;
    }
}
