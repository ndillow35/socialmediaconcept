import * as React from 'react';
import { SideNavMenu } from './SideNavMenu';
import { TopNavMenu } from './TopNavMenu';

export class Layout extends React.Component<{}, {}> {
    public render() {
        return <div className='container-fluid'>
            <div className='row'>
                <div className='col-sm-3'>
                    <SideNavMenu />
                </div>
                {/*<div className='col-sm-9'>
                    <TopNavMenu />
                </div>*/}
                <div className='col-sm-9'>
                    {this.props.children}
                </div>
            </div>
        </div>;
    }
}
